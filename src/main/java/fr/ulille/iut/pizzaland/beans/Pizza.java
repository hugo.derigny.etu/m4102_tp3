package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.util.Arrays;

public class Pizza {

    private long id;
    private String name;
    private int[] ingredients;

    public Pizza() { }

    public int[] getIngredients() {
        return ingredients;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setIngredients(int[] ingredients) {
        this.ingredients = ingredients;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static PizzaDto toDto(Pizza p) {
        PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());
        dto.setIngredients(p.getIngredients());

        return dto;
    }

    public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());
        pizza.setIngredients(dto.getIngredients());

        return pizza;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pizza other = (Pizza) obj;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", name=" + name + "ingredients=" + Arrays.toString(ingredients) + "]";
    }

    public static PizzaCreateDto toCreateDto(Pizza pizza) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());

        return dto;
    }

    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());
        return pizza;
    }
}
