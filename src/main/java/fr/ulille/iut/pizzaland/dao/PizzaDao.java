package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredients (pizza_id INTEGER NOT NULL, ingredient_id INTEGER NOT NULL, PRIMARY KEY (pizza_id, ingredient_id))")
    void createAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropPizzaTable();

    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredients")
    void dropAssociationTable();

    @SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);

    @SqlQuery("SELECT * FROM pizzas AS p INNER JOIN PizzaIngredients AS pi ON (p.id = pi.pizza_id) WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(String name);

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void remove(long id);

    @SqlUpdate("INSERT INTO PizzaIngredients VALUES (:pizza, :ingredient)")
    void insertIngredient(long pizza, int ingredient);

//    @SqlQuery("SELECT * FROM pizzas AS p INNER JOIN PizzaIngredients AS pi ON (p.id = pi.pizza_id) INNER JOIN ingredients AS i ON (i.id = pi.ingredient_id)")
    @SqlQuery("SELECT * FROM pizzas INNER JOIN PizzasIngredients")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(long id);

    @Transaction
    default void dropTable() {
        dropAssociationTable();
        dropPizzaTable();
    }

    @Transaction
    default void createTableAndIngredientAssociation() {
        createPizzaTable();
        createAssociationTable();
    }
}
