package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
    public static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        packages("fr.ulille.iut.pizzaland");

        String environment = System.getenv("PIZZAENV");
//        String environment = null;

        if (environment != null && environment.equals("withdb")) {
            LOGGER.info("Loading with database");
            Jsonb jsonb = JsonbBuilder.create();
            try {
                FileReader reader = new FileReader(getClass().getClassLoader().getResource("ingredients.json").getFile());

                List<Ingredient> ingredients = JsonbBuilder.create().fromJson(reader, new ArrayList<Ingredient>() {
                }.getClass().getGenericSuperclass());

                IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);

                ingredientDao.dropTable();
                ingredientDao.createTable();

                for (Ingredient ingredient : ingredients) {
                    ingredientDao.insert(ingredient.getName());
                }

                System.out.println(Arrays.toString(ingredientDao.getAll().toArray()));

                FileReader readerPizza = new FileReader(getClass().getClassLoader().getResource("pizzas.json").getFile());

                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                pizzaDao.dropTable();
                pizzaDao.createTableAndIngredientAssociation();

                List<Pizza> pizzas = JsonbBuilder.create().fromJson(readerPizza, new ArrayList<Pizza>() {
                }.getClass().getGenericSuperclass());


                for (Pizza pizza : pizzas) {
                    System.out.println(pizza);
                    pizzaDao.insert(pizza.getName());
                    for (int id : pizza.getIngredients()) {
                        pizzaDao.insertIngredient(pizza.getId(), id);
                    }
                }
            } catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        }
    }
}