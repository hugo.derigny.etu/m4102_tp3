package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
    private long id;
    private String name;
    private int[] ingredients;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(int[] ingredients) {
        this.ingredients = ingredients;
    }
}
